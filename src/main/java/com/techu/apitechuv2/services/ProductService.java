package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public ProductModel add (ProductModel product) {
        System.out.println("Estoy en add");
        return this.productRepository.save(product);
    }

    public List<ProductModel> findAll(){
        return this.productRepository.findAll();
    }

    public Optional<ProductModel> findById(String id){
        System.out.println("findById()");
        System.out.println("Obteniendo el producto con el id " + id);
        return this.productRepository.findById(id);
    }

    public ProductModel updateById(ProductModel product){
        System.out.println("updateById()");
        System.out.println("Actualizando el producto con el id " + product.getId());
        //this.productRepository.deleteById(product.getId());
        return this.productRepository.save(product);
    }

    public void deleteById(String id){
        System.out.println("deleteById()");
        System.out.println("Borrando el producto con el id " + id);
        this.productRepository.deleteById(id);
    }
}
