package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.repositories.ProductRepository;
import com.techu.apitechuv2.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public UserModel add (UserModel user) {
        System.out.println("Estoy en add user");
        return this.userRepository.save(user);

    }

    public List<UserModel> findAll(){
        return this.userRepository.findAll();
    }

    public List<UserModel> findAllOrdered (String orderBy){
        System.out.println("findAllOrdered");
        System.out.println("El orden es:" + orderBy);
        List<UserModel> result;
        if (orderBy!=null){
            result = this.userRepository.findAll(Sort.by("age"));
        } else{
            result = this.userRepository.findAll();
        }
        return result;
    }

    public Optional<UserModel> findById(String id){
        System.out.println("findById()");
        System.out.println("Obteniendo el usuario con el id " + id);
        return this.userRepository.findById(id);
    }

    public UserModel updateById(UserModel user){
        System.out.println("updateById()");
        System.out.println("Actualizando el usuario con el id " + user.getId());
        //this.productRepository.deleteById(product.getId());
        return this.userRepository.save(user);
    }

    public void deleteById(String id){
        System.out.println("deleteById()");
        System.out.println("Borrando el usuario con el id " + id);
        this.userRepository.deleteById(id);
    }

}
