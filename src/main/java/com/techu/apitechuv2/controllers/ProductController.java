package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.services.ProductService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping ("/apitechu/v2")

public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public List<ProductModel> getProducts(){
        System.out.println("getProducts");
        return this.productService.findAll();
    }

    @GetMapping ("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("La id de producto a buscar es: "+ id);
        Optional<ProductModel> result = this.productService.findById(id);
        if (result.isPresent() == true){
            return new ResponseEntity<>(result.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping ("/products")
    public ResponseEntity<ProductModel> addProduct (@RequestBody ProductModel product){
        System.out.println("addProducts");
        System.out.println("La id de producto que se va a crear es: "+ product.getId());
        System.out.println("La descripción de producto que se va a crear es: "+ product.getDesc());
        System.out.println("El precio del producto que se va a crear es: "+ product.getPrice());
        return new ResponseEntity<>(this.productService.add(product), HttpStatus.CREATED);
    }

    @PutMapping ("/products/{id}")
    public ResponseEntity<Object> updateProduct (@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("updateProduct()");
        System.out.println("El id del la URL recibida es: " + id);
        System.out.println("El ID del producto a actualizar es: "+ product.getId());
        System.out.println("La descripcion del producto actualizado es: "+ product.getDesc());
        System.out.println("El precio del producto actualizado es: "+ product.getPrice());
        Optional<ProductModel> result = this.productService.findById(id);
        if (result.isPresent() == true){
            this.productService.updateById(product);
            return new ResponseEntity<>("Producto con Id: "+id+ " actualizado." , HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Producto con Id: "+id+ " no existe.", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping ("/products/{id}")
    public ResponseEntity<String> deleteProduct (@PathVariable String id) {
        System.out.println("deleteProduct()");
        System.out.println("El id del la URL para borrar es: " + id);
        Optional<ProductModel> result = this.productService.findById(id);
        if (result.isPresent() == true){
            this.productService.deleteById(id);
            return new ResponseEntity<>("Producto con Id: "+id+ " borrado." , HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Producto con Id: "+id+ " no existe.", HttpStatus.NOT_FOUND);
        }
    }
}