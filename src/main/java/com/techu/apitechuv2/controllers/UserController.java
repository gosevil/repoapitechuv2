package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.services.ProductService;
import com.techu.apitechuv2.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

   @Autowired
   UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>>getUsers(@RequestParam (name = "$orderby", required = false) String orderBy){
        System.out.println("getUsers()");
        return new ResponseEntity<>(this.userService.findAllOrdered(orderBy), HttpStatus.OK);
    }

    @GetMapping ("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById()");
        System.out.println("La id de usuario a buscar es: "+ id);
        Optional<UserModel> result = this.userService.findById(id);
        if (result.isPresent() == true){
            return new ResponseEntity<>(result.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser (@RequestBody UserModel user){
        System.out.println("addUsers");
        System.out.println("La id de usuario que se va a crear es: "+ user.getId());
        System.out.println("El nombre del usuario que se va a crear es: "+ user.getName());
        System.out.println("La edad del usuario que se va a crear es: "+ user.getAge());
        return new ResponseEntity<>(this.userService.add(user), HttpStatus.CREATED);
    }


    @PutMapping ("/users/{id}")
    public ResponseEntity<Object> updateUser (@RequestBody UserModel user, @PathVariable String id){
        System.out.println("updateUser()");
        System.out.println("El id del la URL recibida es: " + id);
        System.out.println("El ID del usuario a actualizar es: "+ user.getId());
        System.out.println("El nombre del usuario actualizado es: "+ user.getName());
        System.out.println("La edad del usuario actualizado es: "+ user.getAge());

        Optional<UserModel> result = this.userService.findById(id);
        if (result.isPresent() == true){
            this.userService.updateById(user);
            return new ResponseEntity<>("Usuario con Id: "+id+ " actualizado." , HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Usuario con Id: "+id+ " no existe.", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping ("/users/{id}")
    public ResponseEntity<String> deleteUser (@PathVariable String id) {
        System.out.println("deleteUser()");
        System.out.println("El id del la URL para borrar es: " + id);
        Optional<UserModel> result = this.userService.findById(id);
        if (result.isPresent() == true){
            this.userService.deleteById(id);
            return new ResponseEntity<>("Usuario con Id: "+id+ " borrado." , HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Usuario con Id: "+id+ " no existe.", HttpStatus.NOT_FOUND);
        }
    }
}
