package com.techu.apitechuv2.repositories;



import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.techu.apitechuv2.models.UserModel;

@Repository
public interface UserRepository extends MongoRepository<UserModel, String> {

}
